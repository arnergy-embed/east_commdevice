/*
 * ModbusMaster.c
 * library for communicating with Modbus slaves over RS232/485 (via RTU protocol).
 *
 *
 *  Created on: 24 Apr 2020
 *      Author: Victor
 */

#include <modbusmaster.h>
#include "main.h"
#include "string.h"

extern UART_HandleTypeDef huart6;



uint8_t setTransmitBuffer(uint8_t u8Index, uint16_t u16Value)
{
  if (u8Index < ku8MaxBufferSize)
  {
    _u16TransmitBuffer[u8Index] = u16Value;
    return ku8MBSuccess;
  }
  else
  {
    return ku8MBIllegalDataAddress;
  }
}


void clearTransmitBuffer()
{
  uint8_t i;

  for (i = 0; i < ku8MaxBufferSize; i++)
  {
    _u16TransmitBuffer[i] = 0;
  }
}


uint16_t getResponseBuffer(uint8_t u8Index)
{
  if (u8Index < ku8MaxBufferSize)
  {
    return _u16ResponseBuffer[u8Index];
  }
  else
  {
    return 0xFFFF;
  }
}


void clearResponseBuffer()
{
  uint8_t i;

  for (i = 0; i < ku8MaxBufferSize; i++)
  {
    _u16ResponseBuffer[i] = 0;
  }
}




uint8_t readDiscreteInputs(uint8_t SlaveAddress,uint16_t u16ReadAddress,uint16_t u16BitQty)
{
	u8MBSlave = SlaveAddress;
  _u16ReadAddress = u16ReadAddress;
  _u16ReadQty = u16BitQty;
  return ModbusMasterTransaction(ku8MBReadDiscreteInputs);
}


uint8_t readInputRegisters(uint8_t SlaveAddress,uint16_t u16ReadAddress, uint8_t u16ReadQty)
{
	u8MBSlave = SlaveAddress;
  _u16ReadAddress = u16ReadAddress;
  _u16ReadQty = u16ReadQty;
  return ModbusMasterTransaction(ku8MBReadInputRegisters);
}


uint8_t writeSingleRegister(uint8_t SlaveAddress,uint16_t u16WriteAddress, uint16_t u16WriteValue)
{
	u8MBSlave = SlaveAddress;
  _u16WriteAddress = u16WriteAddress;
  _u16WriteQty = 0;
  _u16TransmitBuffer[0] = u16WriteValue;
  return ModbusMasterTransaction(ku8MBWriteSingleRegister);
}


uint8_t writeMultipleRegisters(uint8_t SlaveAddress,uint16_t u16WriteAddress,uint16_t u16WriteQty)
{
   u8MBSlave = SlaveAddress;
  _u16WriteAddress = u16WriteAddress;
  _u16WriteQty = u16WriteQty;
  return ModbusMasterTransaction(ku8MBWriteMultipleRegisters);
}



uint8_t ModbusMasterTransaction(uint8_t u8MBFunction)
{
  uint8_t u8ModbusADU[256];
  uint8_t u8ModbusADUSize = 0;
  uint8_t i;
  uint16_t u16CRC;
  uint8_t u8MBStatus = ku8MBSuccess;

  // assemble Modbus Request Application Data Unit
  u8ModbusADU[u8ModbusADUSize++] = u8MBSlave;
  u8ModbusADU[u8ModbusADUSize++] = u8MBFunction;

  switch(u8MBFunction)
  {
    case ku8MBReadDiscreteInputs:
    case ku8MBReadInputRegisters:
      u8ModbusADU[u8ModbusADUSize++] = highByte(_u16ReadAddress);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16ReadAddress);
      u8ModbusADU[u8ModbusADUSize++] = highByte(_u16ReadQty);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16ReadQty);
      break;
  }

  switch(u8MBFunction)
  {
    case ku8MBWriteSingleRegister:
    case ku8MBWriteMultipleRegisters:
      u8ModbusADU[u8ModbusADUSize++] = highByte(_u16WriteAddress);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16WriteAddress);
      break;
  }

  switch(u8MBFunction)
  {
    case ku8MBWriteSingleRegister:
      u8ModbusADU[u8ModbusADUSize++] = highByte(_u16TransmitBuffer[0]);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16TransmitBuffer[0]);
      break;


    case ku8MBWriteMultipleRegisters:

      u8ModbusADU[u8ModbusADUSize++] = highByte(_u16WriteQty);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16WriteQty);
      u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16WriteQty << 1);

      for (i = 0; i < lowByte(_u16WriteQty); i++)
      {
        u8ModbusADU[u8ModbusADUSize++] = highByte(_u16TransmitBuffer[i]);
        u8ModbusADU[u8ModbusADUSize++] = lowByte(_u16TransmitBuffer[i]);
      }
      break;

  }

  // append CRC
  u16CRC = 0xFFFF;
  for (i = 0; i < u8ModbusADUSize; i++)
  {
    u16CRC = crc16_update(u16CRC, u8ModbusADU[i]);
  }
  u8ModbusADU[u8ModbusADUSize++] = lowByte(u16CRC);
  u8ModbusADU[u8ModbusADUSize++] = highByte(u16CRC);
  u8ModbusADU[u8ModbusADUSize] = 0;


  // transmit request
  preTransmission();

  HAL_UART_Transmit(&huart6, (uint8_t *)u8ModbusADU, u8ModbusADUSize, 1000);

  u8ModbusADUSize = 0;		// flush transmit buffer
  memset(u8ModbusADU, 0, sizeof(u8ModbusADU));



  postTransmission();       // start Receive




HAL_UART_Receive(&huart6, (uint8_t *)u8ModbusADU, 10, ku16MBResponseTimeout);
//u8ModbusADUSize = 10 - huart6.RxXferCount;   //test this to know number of received bytes

    // evaluate slave ID, function code once byte 0 is greater than 0
    if (u8ModbusADU[0] != 0)
  {
      // verify response is for correct Modbus slave
      if (u8ModbusADU[0] != u8MBSlave)
      {
        u8MBStatus = ku8MBInvalidSlaveID;
        return u8MBStatus;
      }

      // verify response is for correct Modbus function code (mask exception bit 7)
      if ((u8ModbusADU[1] & 0x7F) != u8MBFunction)
      {
        u8MBStatus = ku8MBInvalidFunction;
        return u8MBStatus;
      }

      // check whether Modbus exception occurred; return Modbus Exception Code
      if (bitRead(u8ModbusADU[1], 7))
      {
        u8MBStatus = u8ModbusADU[2];
        return u8MBStatus;
      }

      // evaluate returned Modbus function code
  }
    else
    {
      u8MBStatus = ku8MBResponseTimedOut;
    }


  // verify response is large enough to inspect further
  if (!u8MBStatus)
  {
	  u8ModbusADUSize = 8;
    // calculate CRC
    u16CRC = 0xFFFF;
    for (i = 0; i < (u8ModbusADUSize - 2); i++)
    {
      u16CRC = crc16_update(u16CRC, u8ModbusADU[i]);
    }

    // verify CRC
    if ( (lowByte(u16CRC) != u8ModbusADU[u8ModbusADUSize - 2]) || (highByte(u16CRC) != u8ModbusADU[u8ModbusADUSize - 1]) )
    {
      u8MBStatus = ku8MBInvalidCRC;
    }
  }

  // disassemble ADU into words
  if (!u8MBStatus)
  {
    // evaluate returned Modbus function code
    switch(u8ModbusADU[1])
    {
      case ku8MBReadDiscreteInputs:
        // load bytes into word; response bytes are ordered L, H, L, H, ...
        for (i = 0; i < (u8ModbusADU[2] >> 1); i++)
        {
          if (i < ku8MaxBufferSize)
          {
            _u16ResponseBuffer[i] = word(u8ModbusADU[2 * i + 4], u8ModbusADU[2 * i + 3]);
          }

          _u8ResponseBufferLength = i;
        }

        // in the event of an odd number of bytes, load last byte into zero-padded word
        if (u8ModbusADU[2] % 2)
        {
          if (i < ku8MaxBufferSize)
          {
            _u16ResponseBuffer[i] = word(0, u8ModbusADU[2 * i + 3]);
          }

          _u8ResponseBufferLength = i + 1;
        }
        break;


      case ku8MBReadInputRegisters:
        // load bytes into word; response bytes are ordered H, L, H, L, ...
        for (i = 0; i < (u8ModbusADU[2] >> 1); i++)
        {
          if (i < ku8MaxBufferSize)
          {
            _u16ResponseBuffer[i] = word(u8ModbusADU[2 * i + 3], u8ModbusADU[2 * i + 4]);
          }

          _u8ResponseBufferLength = i;
        }
        break;
    }
  }

  _u8ResponseBufferIndex = 0;
  return u8MBStatus;
}




/**
    Optimized CRC-16 calculation.

    Polynomial: x^16 + x^15 + x^2 + 1 (0xa001)<br>
    Initial value: 0xffff

    This CRC is normally used in disk-drive controllers.

    The following is the equivalent functionality written in C.

 **/
    uint16_t crc16_update(uint16_t crc, uint8_t a)
    {
	int i;

	crc ^= a;
	for (i = 0; i < 8; ++i)
	{
	    if (crc & 1)
		crc = (crc >> 1) ^ 0xA001;
	    else
		crc = (crc >> 1);
	}

	return crc;
    }


   uint16_t word(uint8_t h, uint8_t l)
   {
	   uint16_t mask = 0x0000;
	   mask = (mask | h) << 8;
	   mask = mask | l;
	   return mask;

   }

