/*
 * eastcomm.c
 *library for communicating with East integrated inverter with MODbus protocol (ver ----).
 *
 *  Created on: 12 May 2020
 *      Author: Victor
 */

#include <eastcomm.h>
#include <modbusmaster.h>
#include <cmsis_os.h>

osThreadId InverterTaskHandle;

uint8_t inverterCount = 0;
uint8_t inverterAddress[4];

//Read Input Registers
 uint16_t Read_Command[57] = {0x0003,0x0006,0x0007,0x0008,0x0009,0x00014,0x00018,0x00019,0x0001A,0x00027,0x0002A, 0x0002B, 0x0002C,
                              0x0002D, 0x00030, 0x00031, 0x00032, 0x00033, 0x00036, 0x00037, 0x00038, 0x00039, 0X0003F, 0X00040, 0x00043,
                              0x00044, 0x00045, 0x00046, 0x00048, 0x00049, 0x0004A, 0x0004B, 0x0004C, 0x0004E, 0x0004F, 0x00050, 0x00052,
						      0x00054, 0x00057, 0x00058, 0x00059, 0x0005A, 0x0005B, 0x0005C, 0x0005D, 0x0005E, 0x0005F, 0x00060, 0x00067,
							  0x00068, 0x00069, 0x0006A, 0x0006B, 0X0006C, 0x0006D, 0x0006E, 0x0006F };

//Multiplier for input registers
const float Multiplier[57] = {0.1, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.01, 0.01, 0.1, 0.1, 0.1, 0.01, 0.1, 0.1, 0.1, 0.01, 0.1, 0.1, 0.1,
							  0.01, 0.01, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.01, 0.1, 0.1, 0.1,
							  0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};

//Registers to read Device parameters
const uint16_t Device_Params[8] = {0x8000, 0x8001, 0x8002, 0x8003, 0x801C, 0x801D, 0x801E, 0x801F};

typedef struct {
	uint16_t inverterID;
	uint16_t inverterParameters[57];
	float conParameters[57];  	 //Inverter parameter * multiplier
} invData;


invData *dataArray;


uint8_t getInverters()
{
	uint8_t result = 0;
	uint8_t slaveAddress = 0;

	for (slaveAddress=1; slaveAddress<5; slaveAddress++)
	{
		result = readInputRegisters(slaveAddress, Read_Command[0], 1);
		  if (result == ku8MBSuccess)
		  {
			  inverterAddress[inverterCount] = slaveAddress;
			  inverterCount++;
		  }
	}
	return inverterCount;
}


/** The main thread, never returns! it reads inverter data every second */
 void query_inverter_data_thread(void const * argument)
{
	uint8_t count,Lcount;
	uint8_t result;

	// Get Inverter Address into structure......
	for(count=0; count<inverterCount; count++)
			{
		      result = readInputRegisters(inverterAddress[count], Device_Params[1], 1);
		      dataArray[count].inverterID = getResponseBuffer(1);
		      osDelay(1000);

			}
	for(;;)
	{
		for(count=0; count<inverterCount; count++)
		{
			for(Lcount=0; Lcount<57; Lcount++)
			{
				result = readInputRegisters(inverterAddress[count], Read_Command[Lcount], 1);

				if (result == ku8MBSuccess)
				 {
				dataArray[count].inverterParameters[Lcount] =  getResponseBuffer(1);    //queue should be used here so multiple functions can write to Modbus
				dataArray[count].conParameters[Lcount] = (dataArray[count].inverterParameters[Lcount]) * Multiplier[Lcount];
				 }
				osDelay(1000);
			}
			osDelay(1000);
		}
	}

}



void initInverterComms(void)
{
	dataArray = (int*)pvPortMalloc(inverterCount * sizeof(invData));

	/* Get_Inverter_Data query function task definition*/
	osThreadDef(Query_Inverter_Data, query_inverter_data_thread, osPriorityNormal, 0, 256);
	InverterTaskHandle = osThreadCreate(osThread(Query_Inverter_Data), NULL);
}






