
#ifndef __MODBUSMASTER_H
#define __MODBUSMASTER_H

#include "stm32f4xx_hal.h"
/**
library for communicating with Modbus slaves over
RS232/485 (via RTU protocol).
*/
#define preTransmission()	  HAL_GPIO_WritePin(INV2_RTS_GPIO_Port, INV2_RTS_Pin, GPIO_PIN_SET);
#define postTransmission()	  HAL_GPIO_WritePin(INV2_RTS_GPIO_Port, INV2_RTS_Pin, GPIO_PIN_RESET);

    // Modbus exception codes

#define ku8MBIllegalFunction      0x01
#define ku8MBIllegalDataAddress   0x02
#define ku8MBIllegalDataValue     0x03
#define ku8MBSlaveDeviceFailure   0x04

    /**
    ModbusMaster success.
    */
#define ku8MBSuccess        		0x00
#define ku8MBInvalidSlaveID   		0xE0
#define ku8MBInvalidFunction        0xE1
#define ku8MBResponseTimedOut       0xE2
#define ku8MBInvalidCRC             0xE3

// Modbus function codes for bit access
#define ku8MBReadDiscreteInputs      0x02 ///< Modbus function 0x02 Read Discrete Inputs
#define ku8MBReadInputRegisters      0x04 ///< Modbus function 0x04 Read Input Registers
#define ku8MBWriteSingleRegister     0x06 ///< Modbus function 0x06 Write Single Register
#define ku8MBWriteMultipleRegisters  0x10 ///< Modbus function 0x10 Write Multiple Registers


#define  ku8MaxBufferSize 	   64
#define ku16MBResponseTimeout  2000             // Modbus timeout [milliseconds]


 uint8_t u8MBSlave;
 uint16_t _u16ReadAddress;
 uint16_t _u16ReadQty;
 uint16_t _u16ResponseBuffer[ku8MaxBufferSize];
 uint16_t _u16WriteAddress;
 uint16_t _u16WriteQty;
 uint16_t _u16TransmitBuffer[ku8MaxBufferSize];
 uint8_t _u8ResponseBufferIndex;
 uint8_t _u8ResponseBufferLength;


uint16_t getResponseBuffer(uint8_t);
void clearResponseBuffer();
uint8_t setTransmitBuffer(uint8_t, uint16_t);
void clearTransmitBuffer();



uint8_t  readDiscreteInputs(uint8_t,uint16_t, uint16_t);
uint8_t  readInputRegisters(uint8_t,uint16_t, uint8_t);
uint8_t  writeSingleRegister(uint8_t,uint16_t, uint16_t);
uint8_t  writeMultipleRegisters(uint8_t,uint16_t, uint16_t);

// master function that conducts Modbus transactions
uint8_t ModbusMasterTransaction(uint8_t u8MBFunction);

//Peripheral Functions
uint16_t crc16_update(uint16_t crc, uint8_t a);
uint16_t word(uint8_t h, uint8_t l);


#endif /* __MODBUSMASTER_H */


