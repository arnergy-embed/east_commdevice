#ifndef __EASTCOMM_H
#define __EASTCOMM_H

#include "stm32f4xx_hal.h"

/**
library for communicating with East integrated
inverter with MODbus protocol (ver ----).
*/



//function checks number of connected inverters
uint8_t getInverters(void);

//master function for all east Inverter communication
void initInverterComms(void);

void query_inverter_data_thread(void const * argument);


#endif /* __EASTCOMM_H */
